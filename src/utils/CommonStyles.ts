

export const Color = {
  darkInk: '#313047',
  darkTeal: '#004D40',
  corbeau: '#13101F',
  indigo: '#1A237E',
  fadeGreen: '#81C784',
  fadeTeal: '#80CBC4',
  mistLightBlue: '#E1F5FE',
  mistGrey: '#ECEFF1',
  mistLightGrey: '#FAFAFA',
  mistGreen: '#E8F5E9',
  pLaurelGreen: '#A4B494',
  p11Grey: '#BEC5AD',
  pArsenic: '#3B5249',
  pWinterGreen: '#519872',
  pJetPurp: '#34252F',
  pRaisinBlack: '#232420',
  pDarkJungle: '#1B2426',
  vanGoghGreen: '#69C992',
  midoriGreen: '#3EB769',
  warmSun: '#FCC62B',
  aqua: '#24a5db',
};

export const Size = {
};

export const FontSize = {
};

export const IconSize = {

};

export const ComponentSize = {

};

