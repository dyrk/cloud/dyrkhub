import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';
import { Color } from './utils/CommonStyles';

// Generate Sales Data
function createData(time: string, amount?: number) {
  return { time, amount };
}

const data = [
  createData('00:00', 0),
  createData('03:00', 0.2),
  createData('06:00', 0.5),
  createData('09:00', 0.9),
  createData('12:00', 1.4),
  createData('15:00', 1.2),
  createData('18:00', 1.0),
  createData('21:00', 0.8),
  createData('24:00', undefined),
];

type Props = {
  chartData: Array<{time: string | undefined, amount: number | undefined}>;
}; 

export const Chart: React.FC<Props> = (props) => {
  const theme = useTheme();

  return (
    <React.Fragment>
      <Title>Broccoli</Title>
      <ResponsiveContainer>
        <LineChart
          data={props.chartData}
          // data={data}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis
            dataKey="time"
            stroke={theme.palette.text.secondary}
            style={theme.typography.body2}
            minTickGap={20}
          />
          <YAxis
            stroke={theme.palette.text.secondary}
            style={theme.typography.body2}
          >
            <Label
              angle={270}
              position="left"
              style={{
                textAnchor: 'middle',
                fill: theme.palette.text.primary,
                ...theme.typography.body1,
              }}
            >
              {"Temperature ℃"}
            </Label>
          </YAxis>
          <Line
            isAnimationActive={false}
            type="monotone"
            dataKey="amount"
            stroke={theme.palette.primary.main}
            strokeWidth={2}
            dot={false}
          />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}