import React, { useState } from 'react'
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import { Color } from '../utils/CommonStyles';
import LoadingButton from '@mui/lab/LoadingButton';
import { Link, useNavigate } from 'react-router-dom';
import TextField from '@mui/material/TextField';

const correctUsername = "dyrkuser";
const correctPassword = "password";

const LoginPage = () => {
    const routerNavigation = useNavigate(); 
    const [usernameInput, setUsernameInput] = useState<string|null>(null);
    const [passwordInput, setPasswordInput] = useState<string|null>(null);
    const [usernameError, setUsernameError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const mdTheme = createTheme({
        palette: {
          primary: {
            main: Color.midoriGreen
          }
        }
      });

    const handleUsernameChange = (username: string) => {
        // console.log("USERNAME: ", username);
        if(usernameError){ // remove the error warning if user is re-entering info
            setUsernameError(false);
        }
        setUsernameInput(username)
        }
    const handlePasswordChange = (password: string) => {
        // console.log("Password: ", username);
        if(passwordError){ // remove the error warning if user is re-entering info
            setPasswordError(false);
        }
        setPasswordInput(password)
        }

    const handleSubmit = ( event: any) => {
        event.preventDefault(); //Preventing the page from reloading which is default behaviour when onSubmit is used
        console.log(`Submit Pressed with input: Username: ${usernameInput} Password: ${passwordInput} `);


        if (usernameInput === correctUsername && passwordInput === correctPassword) {
            // successful login.
            routerNavigation("/dashboard");
        } else { 
            // unsucessful login - find and display the error to the user.
            if (usernameInput !== correctUsername) {
                setUsernameError(true);
            }
            if (passwordInput !== correctPassword) {
                setPasswordError(true);
            }
        }
    }

    return (
        <ThemeProvider theme={mdTheme}>
          <Container 
            maxWidth="lg" 
            sx={{
                  height: "100vh",
                  width: "100vw",
                  display: "flex", 
                  flexDirection: "column",
                  justifyContent: "center", 
                  alignItems: "center", 
                }}
          >
            <Typography color={Color.midoriGreen} component="h1" sx={{fontSize: 40, fontWeight: "bold", textAlign: "center", m: 5}} >
            {"DyrkHub"}
            </Typography>
            <Paper 
                component="form" // Forcing the Paper component to be a 'form' html element
                id="login-form"
                onSubmit={handleSubmit} // Because prop: component = 'form' - onSubmit can be used and triggered by a child component.
                sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    height: "40vh",
                    minHeight: 300,
                    width: "30vw",
                    minWidth: 200,
                    backgroundColor: Color.mistGrey,
                    justifyContent: "space-evenly", 
                    alignItems: "center", 
                    }}
             >
            <Typography component="h1" sx={{fontSize: 20, fontWeight: "bold", textAlign: "center"}} >
                {"Login"}
            </Typography>
            <TextField 
                id="username"
                error={usernameError}
                helperText={"Incorrect Username"} 
                label="Username" 
                variant="outlined" 
                onChange={(event) => handleUsernameChange(event.target.value)} 
            />
            <TextField 
                id="password" 
                error={passwordError}
                helperText={"Incorrect Password"} 
                type="password" 
                label="Password" 
                variant="outlined" 
                onChange={(event) => handlePasswordChange(event.target.value)} 
            />

            {/* <LoadingButton component={Link} sx={{width: 150}} variant="contained" to={"/dashboard"}>Submit</LoadingButton> */}
            <LoadingButton 
                type="submit" // By making the type prop = to 'submit' this means the onClick function of this button will make the parent 'form' component trigger its onSubmit function
                form="login-form" // target the form component with this id. 
                sx={{width: 150}}
                variant="contained"
            >
                    Submit
            </LoadingButton>
            </Paper>
            {/*
            <Typography component="h1" sx={{fontSize: 20, fontWeight: "bold", textAlign: "center"}} >
                {"Login"}
            </Typography>
            <LoadingButton component={Link} sx={{width: 150}} variant="contained" to={"/dashboard"}>Submit</LoadingButton>      */}
          </Container> 
        </ThemeProvider>
    )
}

export default LoginPage
