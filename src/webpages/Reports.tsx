import React from 'react'
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

const ReportsPage = () => {
    return (
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4, ml: 4 }}>
          <Typography component="h1" sx={{fontSize: 20, fontWeight: "bold"}} >
            {"Reports"}
          </Typography>
      </Container> 
    )
}

export default ReportsPage
