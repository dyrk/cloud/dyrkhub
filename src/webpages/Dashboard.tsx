import React from 'react'
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import LoadingButton from '@mui/lab/LoadingButton';
import {Chart} from '../Chart';
import Typography from '@mui/material/Typography';

const DashboardPage = () => {
    const [loading, setLoading] = React.useState(false);
    // const [chartData, setChartData] = React.useState(dummyData);
    const [chartData, setChartData] = React.useState<Array<{time: string | undefined, amount: number | undefined}>>([{time: undefined, amount: undefined}]);
    const [fetchData, setFetchData] = React.useState("");
  
    const getData = async () => {
      setLoading(true);
      console.log("GET DATA");
      const response = await fetch("http://dyrk.io:5000/getdata");
      console.log("Response =", response);
      const json = await response.json();
      console.log("JSON =",json.data);
      setFetchData(json.data);
      const myArray = JSON.parse(json.data);
      console.log("My Array = ", myArray);
      const parsedDataForChart = processChartData(myArray);
      setChartData(parsedDataForChart);
      setLoading(false);
    }
  
    const processChartData = (arrayFromFetch: any[]) => {
      const startTime = arrayFromFetch[0].timestamp;
      console.log("Start time: ", startTime);
  
      const processedChartData: Array<{time: string | undefined, amount: number | undefined}> = arrayFromFetch.map((item) => {
        return {time: formatTimeFromTimestamp((item.timestamp - startTime)), amount: item.data.temperature};
      })
  
      console.log(" PARSED CHART DATA: - ", processedChartData)
  
      return processedChartData;
  
    };
  
    const formatTimeFromTimestamp = (timestamp: number) => {
      // NOTE - For this dummy data we assume the timestamp is in minutes (because it's measuring temperature)
      let formattedString: string;
  
      if (timestamp < 60) {
        if(timestamp < 10){
          formattedString = "00:0" + timestamp.toFixed(0);  
        } else {
          formattedString = "00:" + timestamp.toFixed(0);
        }
      } else { // Over 60
        const hours = Math.floor(timestamp / 60);
        const minutesRemainder = timestamp - (hours * 60);
        let formattedHours: string;   
        let formattedMinutes: string;
        
        if (hours < 10){
          formattedHours = "0" + hours;
        } else {
          formattedHours = hours.toFixed(0);
        }
        if (minutesRemainder < 10){
          formattedMinutes = "0" + minutesRemainder;
        } else {
          formattedMinutes = minutesRemainder.toFixed(0);
        }
  
        formattedString = formattedHours + ":" + formattedMinutes;
      }
      return formattedString
    }
  

    return (
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              {/* Chart*/ }
              <Grid item xs={12} md={8} lg={9}>
                <Paper
                  sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    height: 240,
                  }}
                >
                  <Chart chartData={chartData} />
                </Paper>
              </Grid>
              <Grid item xs={12} md={4} lg={3}>
                <Paper
                  sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    height: 240,
                  }}
                >
              <Typography component="p" >
                {"Data points: " + chartData.length}
              </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column', height: 300, alignItems: "center", justifyContent: "center" }}>
                
                <LoadingButton sx={{width: 150}} variant="contained" onClick={getData} loading={loading}>Get Data</LoadingButton>
                </Paper>
              </Grid>
            </Grid>
            {/* <Copyright sx={{ pt: 4 }} /> */}
          </Container> 
    )
}

export default DashboardPage